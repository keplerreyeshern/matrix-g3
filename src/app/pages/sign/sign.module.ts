import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignRoutingModule } from './sign-routing.module';
import { SignComponent } from './sign.component';
import { LoginComponent } from './login/login.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";


@NgModule({
  declarations: [
    SignComponent,
    LoginComponent
  ],
    imports: [
      CommonModule,
      SignRoutingModule,
      NgxSpinnerModule,
      FormsModule,
      NgbModule,
      FontAwesomeModule
    ]
})
export class SignModule { }
