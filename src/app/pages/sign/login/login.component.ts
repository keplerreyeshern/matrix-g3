import { Component, OnInit } from '@angular/core';
import { Alert } from "../../../interfaces/alert";
import { Router } from "@angular/router";
import { AuthService } from "../../../services/auth/auth.service";
import { NgxSpinnerService } from "ngx-spinner";
import { NgForm } from "@angular/forms";
import { User } from "../../../interfaces/user";
import { faEye, faEyeSlash } from "@fortawesome/free-regular-svg-icons";
import { StorageService } from "../../../services/storage/storage.service";
import { DataService } from "../../../services/data/data.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  user: User=<User>{};
  alert:Alert = <Alert>{};
  type = 'password';
  faPassword = faEye;

  constructor(private loading: NgxSpinnerService,
              private authService: AuthService,
              private dataService: DataService,
              private router: Router,
              private storage: StorageService) {}

  ngOnInit(): void {
    this.alert.type = 'danger';
  }

  submit(form: NgForm){
    this.loading.show();
    let params = {
      email: form.value.email,
      password: form.value.password
    }
    this.authService.login(params).subscribe( response => {
      if (response.ok){
        this.authService.signIn();
        this.storage.setToken(response.token);
        this.getUser(response.token);
      } else {
        this.alert = {
          type: 'danger',
          active: true,
          message: response.message
        }
      }
      this.loading.hide();
    }, error => {
      if (error.status == 500){
        this.alert = {
          type: 'danger',
          active: true,
          message: 'Se encontro un error en el servidor, comunicate con el Administrador del web site'
        }
      } else {
        this.alert = {
          type: 'danger',
          active: true,
          message: 'Se encontro un error, comunicate con el Administrador del web site'
        }
      }
      this.loading.hide();
    });

  }

  getUser(token: string){
    this.dataService.getUser(token).subscribe(response => {
      if (response.ok){
        this.storage.setUser(response.user);
        if (response.user.profile == 'admin' || response.user.profile == 'super_admin') {
          this.router.navigateByUrl('/admin');
        }
      } else {
        this.alert = {
          type: 'danger',
          active: true,
          message: 'Se encontro un error en el servidor, comunicate con el Administrador del web site'
        }
      }
    }, error => {
      if (error.status == 500){
        this.alert = {
          type: 'danger',
          active: true,
          message: 'Se encontro un error en el servidor, comunicate con el Administrador del web site'
        }
      } else {
        this.alert = {
          type: 'danger',
          active: true,
          message: 'Se encontro un error, comunicate con el Administrador del web site'
        }
      }
      console.log(error);
      this.loading.hide();
    });
  }

  togglePassword(){
    if (this.type == 'password'){
      this.type = 'text';
      this.faPassword = faEyeSlash;
    } else {
      this.type = 'password';
      this.faPassword = faEye;
    }
  }

  setTime(){
    setTimeout(() => {
      this.alert.active = false;
    },10000);
  }

  closeAlert() {
    this.alert.active = false;
  }

}
