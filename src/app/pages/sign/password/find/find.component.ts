import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { PasswordService } from "../../../../services/auth/password.service";
import { AuthService } from "../../../../services/auth/auth.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Alert } from "../../../../interfaces/alert";
import { NgForm } from "@angular/forms";
import { User } from "../../../../interfaces/user";
import { StorageService } from "../../../../services/storage/storage.service";

@Component({
  selector: 'app-find',
  templateUrl: './find.component.html',
  styleUrls: ['./find.component.sass']
})
export class FindComponent implements OnInit {

  message = '';
  data: any;
  alert:Alert = <Alert>{};
  disabled = false;
  messagePassword = '';
  alertPassword = false;
  user:User=<User>{};
  token = '';
  password = '';

  constructor(private loading: NgxSpinnerService,
              private service: PasswordService,
              private authService: AuthService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private storage: StorageService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe( params => {
      this.tokenFind(params['token']);
      this.token = params['token'];
    });
  }

  tokenFind(token: string){
    this.loading.show();
    this.service.tokenFind(token).subscribe( response => {
      this.data = response;
      console.log(response);
      if (this.data.message){
        this.message = this.data.message;
        this.alert.active = true;
        this.alert.type = 'success';
      }
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.disabled = true;
        this.loading.hide();
      } else if(err.status == 404){
        this.alert.message = err.error.message;
        this.alert.active = true;
        this.alert.type = 'danger';
        this.disabled = true;
        this.loading.hide();
      } else {
        this.message = 'Se detecto un error comunicate con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.disabled = true;
        this.loading.hide();
      }
    });
  }

  submit(form: NgForm){
    if(form.value.password.length < 8){
      this.alertPassword = true;
      this.messagePassword = 'La contraseña debe tener minimo 8 caracteres';
    } else if (form.value.password != form.value.repeatPassword ){
      this.alertPassword = true;
      this.messagePassword = 'Las contraseñas debe coincidir';
    } else {
      this.password = form.value.password;
      this.loading.show();
      let params = new FormData();
      params.append('Content-Type', 'multipart/form-data');
      params.append('email', this.data.email);
      params.append('token', this.data.token);
      params.append('password', form.value.password);
      this.service.resetPassword(params).subscribe(response => {
        this.user = response;
        this.login();
      }, err => {
        if (err.status == 500) {
          this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
          this.alert.active = true;
          this.alert.type = 'danger';
          this.loading.hide();
        } else if (err.status == 404) {
          this.alert.message = err.error.message;
          this.alert.active = true;
          this.alert.type = 'danger';
          form.resetForm();
          this.loading.hide();
        } else {
          this.alert.message = 'Se detecto un error comunicate con el administrador';
          this.alert.active = true;
          this.alert.type = 'danger';
          this.loading.hide();
        }
      });
    }
  }


  login(){
    const params = {
      grant_type: 'password',
      client_id: '2',
      client_secret: 'tVs301U1jAp0bC2DwrMji3v2jGiKjWexyD6tULIq',
      username: this.user.email,
      password: this.password,
    };

  }

  setTime(){
    setTimeout(() => {
      this.alert.active = false;
    },10000);
  }

  access(){
    if (this.user.active){
      this.router.navigateByUrl('/admin');
      this.authService.signIn();
    } else {
      this.alert.message = 'Tu usuario esta bloqueado y no tienes acceso a la aplicación';
      this.alert.active = true;
      this.alert.type = 'danger';
      this.setTime();
      this.authService.signOut();
    }
    this.loading.hide();
  }

  closeAlert() {
    this.alert.active = false;
  }

}
