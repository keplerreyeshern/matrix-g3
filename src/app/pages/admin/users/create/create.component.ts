import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { Alert } from "../../../../interfaces/alert";
import { NgxSpinnerService } from "ngx-spinner";
import { UsersService } from "../../../../services/admin/users.service";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { faHandPointer } from "@fortawesome/free-solid-svg-icons";
import { StorageService } from "../../../../services/storage/storage.service";
import { faEye, faEyeSlash } from "@fortawesome/free-regular-svg-icons";
import {ToastService} from "../../../../services/components/toast.service";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.sass']
})
export class CreateComponent implements OnInit {

  faHandPointer = faHandPointer;
  type = 'password';
  faPassword = faEye

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private service: UsersService,
              private toastService: ToastService,
              private router: Router,
              private storage: StorageService) {
    this.titleService.setTitle("Nuevo Usuario");
  }

  ngOnInit(): void {

  }

  submit(form: NgForm){
    if (form.value.password != form.value.passwordRepeat){
      this.showDanger('Las contraseñas deben ser iguales');
    } else {
      this.loading.show();
      let params = new FormData();
      params.append('Content-Type', 'multipart/form-data');
      params.append('name', form.value.name);
      params.append('email', form.value.email);
      params.append('password', form.value.password);
      params.append('profile', 'admin');
      this.service.postUser(params).subscribe( response => {
        console.log(response);
        if (response.ok){
          this.router.navigateByUrl('/admin/users');
        } else if (response.err.keyPattern.email == 1){
          this.showDanger('El email ingresado ya se encuentra registrado');
        } else if (response.err.keyPattern.slug == 1){
          this.showDanger('Verifica tu nombre, ya se encuentra registrado');
        }
        this.loading.hide();
      }, error => {
        console.log(error);
        this.showDanger('Se encontro un error al ingresar al usuario, comunicate con el administrador del web-site');
        this.loading.hide();
      });
    }
  }

  togglePassword(){
    if (this.type == 'password'){
      this.type = 'text';
      this.faPassword = faEyeSlash;
    } else {
      this.type = 'password';
      this.faPassword = faEye;
    }
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
