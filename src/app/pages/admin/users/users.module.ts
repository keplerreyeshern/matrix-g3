import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';
import { CreateComponent } from './create/create.component';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { NgxPaginationModule } from "ngx-pagination";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from "@angular/forms";
import { AdminComponentsModule } from "../../../components/admin/admin-components.module";


@NgModule({
  declarations: [
    UsersComponent,
    ListComponent,
    EditComponent,
    CreateComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    FontAwesomeModule,
    NgxPaginationModule,
    AdminComponentsModule,
    NgbModule,
    FormsModule
  ]
})
export class UsersModule { }
