import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GalleriesRoutingModule } from './galleries-routing.module';
import { GalleriesComponent } from './galleries.component';
import { ListComponent } from "./list/list.component";
import { CreateComponent } from "./create/create.component";
import { EditComponent } from "./edit/edit.component";
import { AdminComponentsModule } from "../../../components/admin/admin-components.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { NgxPaginationModule } from "ngx-pagination";
import { NgxDropzoneModule } from "ngx-dropzone";
import { FormsModule } from "@angular/forms";
import { CKEditorModule } from "@ckeditor/ckeditor5-angular";



@NgModule({
  declarations: [
    GalleriesComponent,
    ListComponent,
    CreateComponent,
    EditComponent
  ],
  imports: [
    CommonModule,
    GalleriesRoutingModule,
    AdminComponentsModule,
    NgbModule,
    FontAwesomeModule,
    NgxPaginationModule,
    NgxDropzoneModule,
    FormsModule,
    CKEditorModule
  ]
})
export class GalleriesModule { }
