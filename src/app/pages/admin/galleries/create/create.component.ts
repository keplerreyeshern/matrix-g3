import { Component, OnInit } from '@angular/core';
import { Alert } from "../../../../interfaces/alert";
import { faList, faWarehouse, faSave, faEye, faEyeSlash, faImages, faHandPointer } from '@fortawesome/free-solid-svg-icons';
import { TranslationService } from "../../../../services/admin/translation.service";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { environment } from "../../../../../environments/environment";
import { Title} from "@angular/platform-browser";
import { GalleriesService } from "../../../../services/admin/galleries.service";

// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.sass']
})
export class CreateComponent implements OnInit {

  faList = faList;
  faHandPointer = faHandPointer;
  faImages = faImages;
  faWarehouse = faWarehouse;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  language = localStorage.getItem('language');
  url_images = environment.baseUrl;
  public Editor = ClassicEditor;
  alert:Alert = <Alert>{};
  image:any;
  imageInit:any;
  thumbnail:any;
  editImage = true;
  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: GalleriesService,
              private loading: NgxSpinnerService,
              private router: Router,
              private titleService: Title) {
    this.titleService.setTitle("Crear Galeria");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){

  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    // @ts-ignore
    let language = <string>this.serviceTranslation.getLang();
    let filesLength = this.files.length.toString();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    if (this.image){
      params.append('image', this.image);
    }
    params.append('language', language);
    params.append('fileslength', filesLength);
    for (let f = 0; f < this.files.length; f++){
      params.append('file'+f, this.files[f]);
    }
    this.service.postGallery(params).subscribe( response => {
      let images = response.images;
      let gallery = response.gallery;
      let galleries = JSON.parse(<string>localStorage.getItem('galleries_admin'));
      if (gallery){
        galleries.push(gallery);
        localStorage.setItem('galleries_admin', JSON.stringify(galleries));
      }
      let imagesAll = JSON.parse(<string>localStorage.getItem('images_admin'));
      for (let i=0; images.length>i; i++){
        images[i].active = true;
        imagesAll.push(images[i]);
        console.log(imagesAll);
      }
      localStorage.setItem('images_admin', JSON.stringify(imagesAll));
      this.loading.hide();
      console.log(response);
      this.router.navigateByUrl('/admin/galleries');
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

  closed() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closed()
    },10000);
  }

}
