import { Component, OnInit } from '@angular/core';
import { faList, faSave, faEye, faEyeSlash, faImages, faHandPointer } from '@fortawesome/free-solid-svg-icons';
import { environment } from "../../../../../environments/environment";
import { Alert } from "../../../../interfaces/alert";
import { TranslationService } from "../../../../services/admin/translation.service";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import { Title } from "@angular/platform-browser";
import { NgForm } from "@angular/forms";
import { NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import { BlogsService } from "../../../../services/admin/blogs.service";
import { ToastService } from "../../../../services/components/toast.service";

// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';



@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.sass']
})
export class CreateComponent implements OnInit {

  faList = faList;
  faHandPointer = faHandPointer;
  faImages = faImages;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  language = localStorage.getItem('language');
  url_images = environment.baseUrl;
  public Editor = ClassicEditor;
  alert:Alert = <Alert>{};
  image:any;
  imageInit:any;
  thumbnail:any;
  editImage = true;
  model: NgbDateStruct = <NgbDateStruct>{};
  date: { year: number; month: number; } | undefined;
  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: BlogsService,
              private toastService: ToastService,
              private loading: NgxSpinnerService,
              private router: Router,
              private titleService: Title,
              private calendar: NgbCalendar) {
    this.titleService.setTitle("Crear Noticia");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){

  }

  selectToday() {
    this.model = this.calendar.getToday();
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    let month = '';
    let day = '';
    let language = this.serviceTranslation.getLang();
    if (form.value.date.month < 10 ){
      month = '0'+form.value.date.month;
    } else {
      month = form.value.date.month;
    }
    if (form.value.date.day < 10){
      day = '0' + form.value.date.day;
    } else {
      day = form.value.date.day;
    }
    let date = form.value.date.year + '-' + month + '-' + day;
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('title', form.value.title);
    params.append('intro', form.value.intro);
    params.append('content', form.value.content);
    params.append('date', date);
    if (this.image){
      params.append('image', this.image);
    }
    params.append('language', language);
    this.service.postBlog(params).subscribe(response => {
      if (response.ok){
        this.showSuccess('El bLog '+ response.blog.title.es + ' se creo con exito');
        this.router.navigateByUrl('/admin/blogs');
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
