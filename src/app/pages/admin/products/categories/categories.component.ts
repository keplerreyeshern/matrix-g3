import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { TranslationService } from "../../../../services/admin/translation.service";
import { Category } from "../../../../interfaces/category";
import { ToastService } from "../../../../services/components/toast.service";
import { NgxSpinnerService } from "ngx-spinner";
import { CategoriesService } from "../../../../services/admin/categories.service";

@Component({
  selector: 'app-product-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.sass']
})
export class CategoriesComponent implements OnInit {

  @Input() parent!: string;
  @Input() categoriesSelect:Category[]=[];
  // @ts-ignore
  @ViewChild(CategoriesComponent) CategoriesComponent: CategoriesComponent;
  categories: Category[] = [];
  @Output() categorySelect: EventEmitter<string>;

  constructor(public serviceTranslation: TranslationService,
              private loading: NgxSpinnerService,
              private serviceCategories: CategoriesService,
              private toastService: ToastService, ) {
    this.categorySelect = new EventEmitter();
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.loading.show();
    this.serviceCategories.getCategoriesChildren(this.parent).subscribe(response => {
      if (response.ok){
        this.categories = response.categories;
        this.activeCategories();
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  activeCategories(){
    for(let p=0; this.categories.length>p; p++){
      for (let l=0; this.categoriesSelect.length>l; l++){
        if(this.categories[p]._id == this.categoriesSelect[l]._id){
          this.categories[p].status = true;
        }
      }
    }
  }

  push(id: string){
    this.categorySelect.emit(id);
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
