import { Component, OnInit } from '@angular/core';
import { faList, faPowerOff, faTrashAlt, faSave, faEye, faEyeSlash, faImages, faHandPointer } from '@fortawesome/free-solid-svg-icons';
import { TranslationService } from "../../../../services/admin/translation.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ActivatedRoute, Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { environment } from "../../../../../environments/environment";
import { Title} from "@angular/platform-browser";
import { ProductsService } from "../../../../services/admin/products.service";
import { Category } from "../../../../interfaces/category";
import { Product } from "../../../../interfaces/product";
import { Gallery } from "../../../../interfaces/gallery";
import { Image } from "../../../../interfaces/image";
import { GalleriesService } from "../../../../services/admin/galleries.service";
import { CategoriesService } from "../../../../services/admin/categories.service";
import { ToastService } from "../../../../services/components/toast.service";

// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  faList = faList;
  faHandPointer = faHandPointer;
  faImages = faImages;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  url_images = environment.baseUrl;
  public Editor = ClassicEditor;
  gallery: Gallery = <Gallery>{};
  images: Image[] = [];
  image:any;
  imageInit:any;
  thumbnail:any;
  product:Product = <Product>{
    name:{
      es: 'name',
      en: 'name'
    },
    description:{
      es: 'description',
      en: 'description'
    },
    details:{
      es: 'details',
      en: 'details'
    }
  };
  categories: Category[]= [];
  categoriesSelect: any[]=[];
  editImage = true;
  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: ProductsService,
              private loading: NgxSpinnerService,
              private toastService: ToastService,
              private categoriesService: CategoriesService,
              private router: Router,
              private titleService: Title,
              private serviceImage: GalleriesService,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe( params => {
      this.getData(params['id']);
    });
  }

  ngOnInit(): void {
  }

  getCategories(){
    this.loading.show();
    this.categoriesService.getCategories().subscribe(response => {
      if (response.ok){
        this.categories = response.categories.filter((item: {parent_id:string}) => item.parent_id == null);
        for (let i=0;this.categories.length>i;i++){
          for (let p=0;this.product.categories.length>p;p++){
            if (this.product.categories[p]._id == this.categories[i]._id){
              this.categories[i].status = true;
            }
          }
        }
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  getData(id: string){
    this.loading.show();
    this.service.getProduct(id).subscribe(response => {
      if (response.ok){
        this.product = response.product;
        for (let c=0;this.product.categories.length>c;c++){
          this.categoriesSelect.push(this.product.categories[c]._id);
        }
        this.getCategories();
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
  }

  onRemove(event: any) {
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    // this.loading.show();
    let language = this.serviceTranslation.getLang();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('description', form.value.description);
    params.append('details', form.value.details);
    params.append('code', form.value.key);
    params.append('model', form.value.model);
    params.append('price', form.value.price);
    params.append('categories', JSON.stringify(this.categoriesSelect));
    params.append('language', language);
    this.service.putProduct(this.product._id, params).subscribe(response => {
      console.log(response);
      if (response.ok){
        this.showSuccess('El producto ' + response.product.name.es + ' se actualizo con exito');
        this.router.navigateByUrl('/admin/products');
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }


  push(id: string){
    let index = this.categoriesSelect.findIndex(item => item == id);
    if (index < 0){
      this.categoriesSelect.push(id);
    } else {
      this.categoriesSelect.splice(index, 1);
    }
    console.log(this.categoriesSelect);
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

  deleteImage(gallery: number){
    this.loading.show();

  }

  activeImage(gallery: number){
    this.loading.show();

  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
