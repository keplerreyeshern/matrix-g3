import { Component, OnInit } from '@angular/core';
import { faList, faSave, faEye, faEyeSlash, faImages, faHandPointer } from '@fortawesome/free-solid-svg-icons';
import { Alert } from "../../../../interfaces/alert";
import { TranslationService } from "../../../../services/admin/translation.service";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { environment } from "../../../../../environments/environment";
import { Title} from "@angular/platform-browser";
import { ProductsService } from "../../../../services/admin/products.service";
import { Category } from "../../../../interfaces/category";

// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {ToastService} from "../../../../services/components/toast.service";
import {CategoriesService} from "../../../../services/admin/categories.service";
import {StorageService} from "../../../../services/storage/storage.service";



@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.sass']
})
export class CreateComponent implements OnInit {

  faList = faList;
  faHandPointer = faHandPointer;
  faImages = faImages;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  url_images = environment.baseUrl;
  public Editor = ClassicEditor;
  alert:Alert = <Alert>{};
  image:any;
  imageInit:any;
  thumbnail:any;
  categories: Category[]= [];
  categoriesSelect: any[]=[];
  editImage = true;
  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private categoriesService: CategoriesService,
              private service: ProductsService,
              private loading: NgxSpinnerService,
              private toastService: ToastService,
              private router: Router,
              private titleService: Title) {
    this.titleService.setTitle("Crear Producto");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.loading.show();
    this.categoriesService.getCategories().subscribe(response => {
      if (response.ok){
        this.categories = response.categories.filter((item: {parent_id:string}) => item.parent_id == null);
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('description', form.value.description);
    params.append('details', form.value.details);
    params.append('code', form.value.key);
    params.append('model', form.value.model);
    params.append('price', form.value.price);
    params.append('categories', JSON.stringify(this.categoriesSelect));
    if (this.image){
      params.append('image', this.image);
    }
    params.append('language', language);
    this.service.postProduct(params).subscribe(response => {
      // console.log(response);
      if (response.ok){
        this.showSuccess('El producto ' + response.product.name.es + ' se creo con exito');
        this.router.navigateByUrl('/admin/products');
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

  closed() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closed()
    },10000);
  }

  push(id: string){
    let index = this.categoriesSelect.findIndex(item => item == id);
    if (index < 0){
      this.categoriesSelect.push(id);
    } else {
      this.categoriesSelect.splice(index, 1);
    }
    console.log(this.categoriesSelect);
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
