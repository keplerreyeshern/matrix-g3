import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsRoutingModule } from './news-routing.module';
import { NewsComponent } from './news.component';
import { ListComponent } from "./list/list.component";
import { CreateComponent } from "./create/create.component";
import { EditComponent } from "./edit/edit.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { NgxPaginationModule } from "ngx-pagination";
import { NgxDropzoneModule } from "ngx-dropzone";
import { FormsModule } from "@angular/forms";
import { CKEditorModule } from "@ckeditor/ckeditor5-angular";
import { AdminComponentsModule } from "../../../components/admin/admin-components.module";


@NgModule({
  declarations: [
    NewsComponent,
    ListComponent,
    CreateComponent,
    EditComponent
  ],
  imports: [
    CommonModule,
    NewsRoutingModule,
    AdminComponentsModule,
    NgbModule,
    FontAwesomeModule,
    NgxPaginationModule,
    NgxDropzoneModule,
    FormsModule,
    CKEditorModule
  ]
})
export class NewsModule { }
