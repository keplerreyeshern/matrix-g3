import { Component, OnInit } from '@angular/core';
import { faTable, faPlus, faPowerOff, faTrashAlt, faCertificate, faStar } from '@fortawesome/free-solid-svg-icons';
import { News } from "../../../../interfaces/news";
import { environment } from "../../../../../environments/environment";
import { Title } from "@angular/platform-browser";
import { NgxSpinnerService } from "ngx-spinner";
import { TranslationService } from "../../../../services/admin/translation.service";
import { NewsService } from "../../../../services/admin/news.service";
import { ToastService } from "../../../../services/components/toast.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faCertificate = faCertificate;
  faStar = faStar;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  news: News[]=[];
  public page: number | undefined;
  url_images = environment.baseUrl;

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private toastService: ToastService,
              private service: NewsService,
              public serviceTranslation: TranslationService) {
    this.titleService.setTitle("Lista de Noticias");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.loading.show();
    this.service.getNews().subscribe(response => {
      if(response.ok){
        this.news = response.news;
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  active(id: string){
    this.loading.show();
    this.service.activeNews(id).subscribe(response => {
      if(response.ok){
        if (response.news.active) {
          this.showSuccess('La noticia ' + response.news.title.es + ' se activo con exito');
        } else {
          this.showSuccess('La noticia ' + response.news.title.es + ' se desactivo con exito');
        }
        this.getData();
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  delete(id: string){
    this.loading.show();
    this.service.deleteNews(id).subscribe(response => {
      if(response.ok){
        this.showSuccess('La noticia ' + response.news.title.es + ' se elimino con exito');
        this.getData();
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }


  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
