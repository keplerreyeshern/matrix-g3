import {Component, HostListener, OnInit} from '@angular/core';
import { Meta, Title} from "@angular/platform-browser";
import { ComponentsService } from "../../services/admin/components.service";
import { Alert } from "../../interfaces/alert";
import { NgxSpinnerService } from "ngx-spinner";
import { StorageService } from "../../services/storage/storage.service";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.sass']
})
export class AdminComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event: Event) {
    if (window.innerWidth < 769) {
      this.serviceMenu.setClose(true);
    } else {
      this.serviceMenu.setClose(false);
    }
  };
  alert:Alert=<Alert>{};

  constructor(private title: Title,
              private meta: Meta,
              private loading: NgxSpinnerService,
              public serviceMenu: ComponentsService,
              private storage: StorageService) {
    this.title.setTitle('Administrador');
  }

  ngOnInit(): void {
  }



  setTime(){
    setTimeout(() => {
      this.alert.active = false;
    },10000);
  }

  closeAlert() {
    this.alert.active = false;
  }

}
