import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminComponentsModule } from "../../components/admin/admin-components.module";
import { SharedModule } from "../../components/shared/shared.module";


@NgModule({
  declarations: [
    AdminComponent,
    DashboardComponent
  ],
    imports: [
      CommonModule,
      AdminRoutingModule,
      NgxSpinnerModule,
      AdminComponentsModule,
      SharedModule
    ]
})
export class AdminModule { }
