import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastsComponent } from "./toasts/toasts.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";



@NgModule({
  declarations: [
    ToastsComponent
  ],
  exports: [
    ToastsComponent
  ],
  imports: [
    CommonModule,
    NgbModule
  ]
})
export class SharedModule { }
