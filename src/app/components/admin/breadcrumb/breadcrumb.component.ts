import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-breadcrumb-admin',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.sass']
})
export class BreadcrumbAdminComponent implements OnInit {

  @Input() titlePage: string = '';
  @Input() backPage: string = '';
  @Input() url: string = '';
  @Input() single: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
