import { Component, OnInit } from '@angular/core';
import { faBars, faPowerOff } from '@fortawesome/free-solid-svg-icons';
import { ComponentsService } from '../../../services/admin/components.service';
import { AuthService } from '../../../services/auth/auth.service';
import { Router } from '@angular/router';
import { User } from "../../../interfaces/user";
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-navbar-admin',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarAdminComponent implements OnInit {

  faPowerOff = faPowerOff;
  faBars = faBars;
  url_image = environment.baseUrl
  subMenu = false;
  user:User = <User>{};

  constructor(public serviceMenu: ComponentsService,
              private serviceAuth: AuthService,
              private router: Router) { }

  ngOnInit(): void {
    this.getUser();
  }

  getUser(){
    this.user = JSON.parse(<string>localStorage.getItem('user'));
  }

  closed(){
    if (this.serviceMenu.getClose()){
      this.serviceMenu.setClose(false);
    } else {
      this.serviceMenu.setClose(true);
    }
  }

  logout(){
    this.serviceAuth.signOut();
    this.router.navigate(['/sign/login']);
  }

}
