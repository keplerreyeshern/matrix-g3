export interface Image {
  id: number;
  title: string;
  image: string;
  name: string;
  description: string;
  active: boolean;
}
