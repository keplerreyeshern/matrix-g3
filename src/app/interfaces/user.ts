export interface User {
  _id: string;
  name: string;
  email: string;
  email_verified_at: string;
  password: string;
  telephone: string;
  pass: string;
  profile: string;
  avatar: string;
  active: boolean;
  remember_token: string;
}
