import { Component } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'Matrix';
  activeLang = 'es';

  constructor(private translate: TranslateService) {
    if (typeof this.activeLang === 'string') {
      this.translate.setDefaultLang(this.activeLang);
      localStorage.setItem('isLoggedIn', 'false');
      localStorage.setItem('language', 'es');
    }
  }
}
