import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {User} from "../../interfaces/user";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrl = environment.baseUrl+ '/users';
  public isLoggedIn  = false;

  constructor(private http: HttpClient) {
  }

  login(params: any){
    return this.http.post<any>(this.baseUrl + '/login', params);
  }

  register(params: any){
    return this.http.post<any>(this.baseUrl + '/create', params);
  }

  signIn() {
    localStorage.setItem('isLoggedIn', 'true');
    this.isLoggedIn = true;
  }

  getIsLoggedIn(): boolean{
    if (!localStorage.getItem('isLoggedIn')){
      return this.isLoggedIn;
    } else {
      return  this.getBoolean(<string>localStorage.getItem('isLoggedIn'));
    }
  }

  signOut() {
    localStorage.clear();
    localStorage.setItem('isLoggedIn', 'false');
    this.isLoggedIn = false;
  }

  getBoolean(option: string): boolean{
    if (option == 'true'){
      return true;
    } else {
      return false;
    }
  }

}
