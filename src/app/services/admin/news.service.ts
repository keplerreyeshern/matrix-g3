import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { StorageService } from "../storage/storage.service";

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  url = environment.baseUrl + '/news';
  token = '';
  headers:any;

  constructor(private http: HttpClient,
              private storage: StorageService) {
    this.token = this.storage.getToken();
    this.headers = new HttpHeaders({
      'x-token': this.token,
    });
  }

  getNews(){
    return this.http.get<any>(this.url, {headers: this.headers});
  }


  showNews(id: string){
    return this.http.get<any>(this.url + '/news/' + id, {headers: this.headers});
  }


  activeNews(id: string){
    return this.http.post<any>(this.url + '/active',  {newsId: id}, {headers: this.headers});
  }

  postNews(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  putNews(id: string, params: any){
    return this.http.put<any>(this.url + '/' + id, params, {headers: this.headers});
  }

  deleteNews(id: string){
    return this.http.delete<any>(this.url + '/' + id, {headers: this.headers});
  }
}
