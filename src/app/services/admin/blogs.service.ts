import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {StorageService} from "../storage/storage.service";

@Injectable({
  providedIn: 'root'
})
export class BlogsService {

  url = environment.baseUrl + '/blogs';
  token = '';
  headers:any;

  constructor(private http: HttpClient,
              private storage: StorageService) {
    this.token = this.storage.getToken();
    this.headers = new HttpHeaders({
      'x-token': this.token,
    });
  }

  getBlogs(){
    return this.http.get<any>(this.url, {headers: this.headers});
  }


  showBlog(id: string){
    return this.http.get<any>(this.url + '/blog/' + id, {headers: this.headers});
  }


  activeBlog(id: string){
    return this.http.post<any>(this.url + '/active',  {blogId: id}, {headers: this.headers});
  }

  postBlog(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  putBlog(id: string, params: any){
    return this.http.put<any>(this.url + '/' + id, params, {headers: this.headers});
  }

  deleteBlog(id: string){
    return this.http.delete<any>(this.url + '/' + id, {headers: this.headers});
  }
}
