import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { StorageService } from "../storage/storage.service";

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  url = environment.baseUrl + '/products';
  token = '';
  headers:any;

  constructor(private http: HttpClient,
              private storage: StorageService) {
    this.token = this.storage.getToken();
    this.headers = new HttpHeaders({
      'x-token': this.token,
    });
  }

  getProducts(){
    return this.http.get<any>(this.url, {headers: this.headers});
  }

  getProduct(id: string){
    return this.http.get<any>(this.url + '/product/' + id, {headers: this.headers});
  }

  getCategories(){
    return this.http.get<any>(this.url + '/categories', {headers: this.headers});
  }

  getCategoriesChild(id: any){
    return this.http.get<any>(this.url + '/categories/children/' + id, {headers: this.headers});
  }


  activeProduct(id: string){
    return this.http.post<any>(this.url + '/active', {productId: id}, {headers: this.headers});
  }

  newProduct(id: string){
    return this.http.post<any>(this.url + '/new', {productId: id}, {headers: this.headers});
  }

  postProduct(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  putProduct(id: any, params: any){
    return this.http.put<any>(this.url + '/' + id, params, {headers: this.headers});
  }

  deleteProduct(id: string){
    return this.http.delete<any>(this.url + '/' + id, {headers: this.headers});
  }
}
