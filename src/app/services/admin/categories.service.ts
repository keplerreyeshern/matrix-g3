import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {StorageService} from "../storage/storage.service";

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  url = environment.baseUrl + '/categories';
  token = '';
  headers:any;

  constructor(private http: HttpClient,
              private storage: StorageService) {
    this.token = this.storage.getToken();
    this.headers = new HttpHeaders({
      'x-token': this.token,
    });
  }


  getCategories(){
    return this.http.get<any>(this.url, {headers: this.headers});
  }

  showCategory(id: string){
    return this.http.get<any>(this.url + '/category/' + id, {headers: this.headers});
  }

  getCategoriesChildren(id: string){
    return this.http.get<any>(this.url + '/children/' + id, {headers: this.headers});
  }

  parentCategory(id: string){
    return this.http.get<any>(this.url + '/parent/' + id, {headers: this.headers});
  }

  activeCategory(id: string){
    return this.http.post<any>(this.url + '/active', {categoryId: id}, {headers: this.headers});
  }

  postCategory(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  putCategory(id: string, params: any){
    return this.http.put<any>(this.url + '/update/' + id, params, {headers: this.headers});
  }

  deleteCategory(id: string){
    return this.http.delete<any>(this.url + '/' + id, {headers: this.headers});
  }
}
